import { fromGlobalId, nodeDefinitions } from 'graphql-relay';

export const { nodeInterface, nodeField } = nodeDefinitions(
    globalId => {
        const { type, id } = fromGlobalId(globalId);
        if(type === 'Employee') {
            return store.hris.employee.getEmployeeById(id);
        }
    }
);