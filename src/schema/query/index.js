import { GraphQLObjectType, GraphQLInt, GraphQLList, GraphQLString, GraphQLBoolean, GraphQLNonNull } from 'graphql';
import { nodeField } from '../node';
import { attendanceSummaryConnection } from '../types/connections/AttendanceConnections';
import { connectionArgs } from 'graphql-relay';
import { EmployeeType } from '../types/EmployeeType';
import { AttendanceSummaryType } from '../types/AttendanceSummaryType';
import { resolver } from 'graphql-sequelize';
import db from '../../db/models';


export const queryType = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    node: nodeField,
    employee: {
      type: EmployeeType,
      args: {
          empId: {
              type: GraphQLInt
          }
      },
      resolve: resolver(db.Employee)
    },
    attendanceSummary: {
      type: AttendanceSummaryType,
      args: {
        empIds: {
            type: new GraphQLList(GraphQLInt),
        },
        isActive: {
            type: GraphQLBoolean,
        },
        compIds: {
            type: new GraphQLList(GraphQLInt),
        },
        divIds: {
            type: new GraphQLList(GraphQLInt),
        },
        deptIds: {
            type: new GraphQLList(GraphQLInt),
        },
        desIds: {
            type: new GraphQLList(GraphQLInt),
        },
        funcIds: {
            type: new GraphQLList(GraphQLInt),
        },
        wbIds: {
            type: new GraphQLList(GraphQLInt),
        },
        fromDate: {
            type: new GraphQLNonNull(GraphQLString),
        },
        toDate: {
            type: new GraphQLNonNull(GraphQLString),
        },
      },
      resolve: resolver(db.AttendanceSummary)
    }
  }),
});