import { globalIdField } from 'graphql-relay';
import { GraphQLObjectType, GraphQLString, GraphQLInt, GraphQLFloat } from 'graphql';
import { nodeInterface } from '../node';

export const AttendanceSummaryType = new GraphQLObjectType({
  name: 'AttendanceSummary',
  description: 'A single user',
  fields: () => ({
    id: globalIdField(),
    empId: {
      type: GraphQLInt
    },
    firstName: {
      type: GraphQLString,
    },
    lastName: {
      type: GraphQLString
    },
    day: {
      type: GraphQLFloat
    },
    hour: {
      type: GraphQLFloat
    },
    late: {
      type: GraphQLFloat
    },
    ut: {
      type: GraphQLFloat
    },
    ob: {
      type: GraphQLFloat
    },
    absent: {
      type: GraphQLFloat
    },
    leave: {
      type: GraphQLFloat
    },
  }),
  isTypeOf: () => AttendanceSummaryType,
});