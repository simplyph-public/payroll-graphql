import { Sequelize } from 'sequelize';

export default (sequelize) => {
  return sequelize.define("time_attendance2", 
    {
      id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        field: 'autoid'
      },
      empId: {
        type: Sequelize.INTEGER,
        field: 'emp_id'
      },
      coveringDay: {
        type: Sequelize.DATEONLY,
        field: 'covering_day'
      },
      day: {
        type: Sequelize.DOUBLE,
        field: 'num_days'
      },
      hour: {
        type: Sequelize.DOUBLE,
        field: 'num_hours'
      },
      late: {
        type: Sequelize.DOUBLE,
        field: 'num_late'
      },
      ut: {
        type: Sequelize.DOUBLE,
        field: 'num_undertime'
      },
      absent: {
        type: Sequelize.DOUBLE,
        field: 'num_absent'
      },
      ot: {
        type: Sequelize.DOUBLE,
        field: 'num_overtime'
      },
      nd: {
        type: Sequelize.DOUBLE,
        field: 'num_nightdiff'
      },
      ob: {
        type: Sequelize.DOUBLE,
        field: 'num_overbreak'
      },
      leave: {
        type: Sequelize.DOUBLE,
        field: 'num_leave'
      },
    },
    {
      timestamps: false,
      freezeTableName: true,      
    }
  );
}