import { Sequelize } from 'sequelize';
import { createContext, EXPECTED_OPTIONS_KEY } from 'dataloader-sequelize';
import path from 'path';
import employeeFactory from './hris/EmployeeModel';
import employeeStatusFactory from './hris/EmployeeStatusModel';
import timeAttendanceFactory from './timekeeping/TimeAttendanceModel';

const config = require(__dirname + '/../config.json')['development'];
const sequelize = new Sequelize(config.url, 
  {
    ...config,
    operatorAliases: Sequelize.Op
  }
);

const Employee = employeeFactory(sequelize);
const EmployeeStatus = employeeStatusFactory(sequelize);
const TimeAttendance = timeAttendanceFactory(sequelize);
const AttendanceSummary = (seq) => seq.define('AttendanceSummary', 
{
  id: {
    primaryKey: true,
    type: Sequelize.INTEGER,
    field: 'empId',
    autoIncrement: true
  },
},
{
  timestamps: false,
  freezeTableName: true,      
});

Employee.associate = () => {
  Employee.hasMany(TimeAttendance, {
    foreignKey: 'empId'
  })
}

TimeAttendance.associate = () => {
  TimeAttendance.belongsToMany(Employee, {through: AttendanceSummary, foreignKey: 'id', otherKey: 'empId'});
}

const db = {
  sequelize,
  Sequelize,
  Employee,
  EmployeeStatus,
  TimeAttendance,
  AttendanceSummary,
}

Object.values(db).forEach((model) => {
  if(model.associate) {
      model.associate(db);
  }
});

const context = createContext(sequelize);

db.context = context;

export default db;