import { Sequelize } from 'sequelize';

export default (sequelize) => {
  return sequelize.define("hris_employee", 
    {
      id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        field: 'emp_id'
      },
      empId: {
        type: Sequelize.INTEGER,
        field: 'emp_id'
      },
      firstName: {
        type: Sequelize.STRING,
        field: 'firstname'
      },
      lastName: {
        type: Sequelize.STRING,
        field: 'lastname'
      },
    },
    {
      timestamps: false,
      freezeTableName: true,      
    }
  );
}