import { Sequelize } from 'sequelize';

export default (sequelize) => {
  return sequelize.define("hris_employee_status", 
    {
      id: {
        primaryKey: true,
        type: Sequelize.INTEGER,
        field: 'status_id'
      },
      statusId: {
        type: Sequelize.INTEGER,
        field: 'status_id'
      },
      desc: {
        type: Sequelize.STRING,
        field: 'status_desc'
      },
      statusType: {
        type: Sequelize.STRING,
        field: 'status_type'
      },
    },
    {
      timestamps: false,
      freezeTableName: true,      
    }
  );
}