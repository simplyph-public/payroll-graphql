import cors from 'cors';
import express from 'express';
import bodyParser from 'body-parser';
import graphqlHTTP from 'express-graphql';
import schema from './schema';
import db from "./db/models";

const port = process.env.PORT || 4000;
const app = express();

app.use(cors());
app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('json spaces', 2);
app.post('/login', (req, res) => {
  db.sequelize.query('SELECT * FROM view_login WHERE isactive = 1 AND username = :username AND pwd = MD5(:password)',
    {
      replacements: req.body,
      type: db.sequelize.QueryTypes.SELECT 
    })
  .then(users => {
    if(users.length > 0) {
      res.json(users[0]);
    } else {
      res.sendStatus(401);
    }
  }).catch(err => {
    res.sendStatus(400);
  });
});

app.listen(port, () => {
  console.log('Server listening on port ' + port);
});
